-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 26 mars 2021 à 02:04
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `albumotek`
--

-- --------------------------------------------------------

--
-- Structure de la table `disk`
--

DROP TABLE IF EXISTS `disk`;
CREATE TABLE IF NOT EXISTS `disk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rayon_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  `number_song` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `realizer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C74DD02D3202E52` (`rayon_id`),
  KEY `IDX_C74DD028B281E60` (`realizer_id`),
  KEY `IDX_C74DD02A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `disk`
--

INSERT INTO `disk` (`id`, `rayon_id`, `name`, `create_at`, `number_song`, `description`, `realizer_id`, `user_id`) VALUES
(11, 8, 'Disque 1', '2021-03-25 09:30:16', 7, 'Description 1 sor l hombre que toco las puertas del casa cuando estuve', 2, 3),
(12, 11, 'Disque 2', '2021-03-25 09:59:36', 6, 'Description 2', 5, 4),
(14, 9, 'Disque 6', '2021-03-25 15:01:42', 4, 'Description 6', 2, 3),
(15, 8, 'Disque 7', '2021-03-25 23:28:32', 7, 'Disque qui parle du chemin de la vie', 2, 3),
(16, 11, 'Disque 8', '2021-03-25 23:42:44', 8, 'Le limon qui donne du sucre au sel', 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210320170422', '2021-03-20 17:04:44', 288),
('DoctrineMigrations\\Version20210321192400', '2021-03-21 19:24:34', 567),
('DoctrineMigrations\\Version20210323153918', '2021-03-23 15:47:44', 350),
('DoctrineMigrations\\Version20210324175935', '2021-03-24 17:59:48', 450),
('DoctrineMigrations\\Version20210324190539', '2021-03-24 19:06:12', 762),
('DoctrineMigrations\\Version20210324191655', '2021-03-24 19:18:05', 201),
('DoctrineMigrations\\Version20210324191705', '2021-03-24 19:18:58', 153),
('DoctrineMigrations\\Version20210325003556', '2021-03-25 00:36:12', 595),
('DoctrineMigrations\\Version20210325084451', '2021-03-25 08:55:44', 236);

-- --------------------------------------------------------

--
-- Structure de la table `rayon`
--

DROP TABLE IF EXISTS `rayon`;
CREATE TABLE IF NOT EXISTS `rayon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `rayon`
--

INSERT INTO `rayon` (`id`, `name`, `description`) VALUES
(8, 'BACHATA', 'La bachata est un rythme dansant originaire de République dominicaine, plein de folklore. On peut le considérer comme un mélange de boléro (surtout, le boléro rythmique) avec des influences musicales d\'origine africaine et d\'autres styles comme le son, le kompa haïtien, le merengue, le cha-cha-cha et le tango'),
(9, 'KIZOMBA', 'Le kizomba est un genre musical et une danse africaine, originaire de l\'Angola , qui est devenue populaire en Europe et dans le Monde.\r\nLe mot kizomba veut dire « fête » en kimbundu, une des principales langues parlées en Angola.'),
(10, 'LATIN JAZZ', 'Le latin jazz (expression anglaise) est une branche du jazz qui puise ses racines dans la fusion des rythmes et des formes originales de la musique latine, en particulier cubaine et brésilienne, avec des éléments de jazz'),
(11, 'POP', 'La musique pop (ou simplement la pop) est un genre musical apparu dans les années 1960 au Royaume-Uni et aux États-Unis. Ces chansons parlent en général de l\'amour ou des relations entre les femmes et les hommes');

-- --------------------------------------------------------

--
-- Structure de la table `realizer`
--

DROP TABLE IF EXISTS `realizer`;
CREATE TABLE IF NOT EXISTS `realizer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk_realizer` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `telephone` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `realizer`
--

INSERT INTO `realizer` (`id`, `name`, `last_name`, `disk_realizer`, `create_at`, `telephone`, `email`, `description`) VALUES
(2, 'Pascal', 'Plateau', 100, '2021-03-23 16:55:57', 784895361, 'pascal@gmail.com', 'super producteur des disques'),
(4, 'Freddy', 'Mercury', 100, '2021-03-23 22:53:15', 784956231, 'freddy@gmail.com', 'Realisateur des queen'),
(5, 'Quincy', 'Jones', 100, '2021-03-23 23:48:13', 785964213, 'quincy', 'Realisateur du disque thriller de Michael Jackson');

-- --------------------------------------------------------

--
-- Structure de la table `song`
--

DROP TABLE IF EXISTS `song`;
CREATE TABLE IF NOT EXISTS `song` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disk_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` double DEFAULT NULL,
  `singer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_33EDEEA163B1F25` (`disk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `song`
--

INSERT INTO `song` (`id`, `disk_id`, `name`, `description`, `duration`, `singer`) VALUES
(3, 12, 'Chanson 1', 'Description 1', 4.5, 'ismel'),
(4, 11, 'Chanson 3', 'Description 3', 6.5, 'ismel'),
(5, 11, 'Chanson 4', 'Description 4', 4, 'ismel'),
(7, 11, 'Chanson 5', 'description 5', 5.2, 'ismel'),
(8, 11, 'Chanson 6', 'Description 6', 5, 'ismel'),
(9, 11, 'Chanson 7', 'description 7', 4, 'ismel');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`) VALUES
(3, 'ismel@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$ZXBFNVo3bm5sSHFBQ28zWA$6YLIVxLu7S7Fvvr4U/E2jjmMap2NCxB63S5AsNm+PX0'),
(4, 'emilie@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$UVh0ODdtLllHd29kTGN6Lg$qwbFZEptuXumsS+Eb9Lt89EJ84kovm1duwe7OuHwIOg'),
(5, 'pascal@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$Qml6QTFaR0tCbldxWkJhRg$xcMibz2GUq5xNb0BClFs1Yd2wIz3R/SSRghSmeOhhfI'),
(6, 'moraima@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$cldqWnRpOGp5ZmdWdXNLYQ$eR+YmJo08jIqb+ScbYMBh/SAg88bJdnWAi6h5t3j3HQ');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `disk`
--
ALTER TABLE `disk`
  ADD CONSTRAINT `FK_C74DD028B281E60` FOREIGN KEY (`realizer_id`) REFERENCES `realizer` (`id`),
  ADD CONSTRAINT `FK_C74DD02A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_C74DD02D3202E52` FOREIGN KEY (`rayon_id`) REFERENCES `rayon` (`id`);

--
-- Contraintes pour la table `song`
--
ALTER TABLE `song`
  ADD CONSTRAINT `FK_33EDEEA163B1F25` FOREIGN KEY (`disk_id`) REFERENCES `disk` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
