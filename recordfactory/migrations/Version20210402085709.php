<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210402085709 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE disk (id INT AUTO_INCREMENT NOT NULL, rayon_id INT NOT NULL, realizer_id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_C74DD02D3202E52 (rayon_id), INDEX IDX_C74DD028B281E60 (realizer_id), INDEX IDX_C74DD02A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rayon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE realizer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, disk_realizer INT NOT NULL, create_at DATETIME NOT NULL, telephone INT NOT NULL, email VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE song (id INT AUTO_INCREMENT NOT NULL, disk_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, duration DOUBLE PRECISION DEFAULT NULL, singer VARCHAR(255) NOT NULL, INDEX IDX_33EDEEA163B1F25 (disk_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE disk ADD CONSTRAINT FK_C74DD02D3202E52 FOREIGN KEY (rayon_id) REFERENCES rayon (id)');
        $this->addSql('ALTER TABLE disk ADD CONSTRAINT FK_C74DD028B281E60 FOREIGN KEY (realizer_id) REFERENCES realizer (id)');
        $this->addSql('ALTER TABLE disk ADD CONSTRAINT FK_C74DD02A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE song ADD CONSTRAINT FK_33EDEEA163B1F25 FOREIGN KEY (disk_id) REFERENCES disk (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE song DROP FOREIGN KEY FK_33EDEEA163B1F25');
        $this->addSql('ALTER TABLE disk DROP FOREIGN KEY FK_C74DD02D3202E52');
        $this->addSql('ALTER TABLE disk DROP FOREIGN KEY FK_C74DD028B281E60');
        $this->addSql('ALTER TABLE disk DROP FOREIGN KEY FK_C74DD02A76ED395');
        $this->addSql('DROP TABLE disk');
        $this->addSql('DROP TABLE rayon');
        $this->addSql('DROP TABLE realizer');
        $this->addSql('DROP TABLE song');
        $this->addSql('DROP TABLE user');
    }
}
