<?php

namespace App\Controller;

use App\Entity\Disk;
use App\Form\SearchAlbumotekType;
use App\Repository\DiskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccueilController extends AbstractController
{
      /**
    * @var DiskRepository
    */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
     private $em;

    public function __construct(DiskRepository $diskrepository ,EntityManagerInterface $em){
         $this->repository=$diskrepository;
         $this->em=$em;
    }

    /**
     * @Route("/accueil", name="accueil_show" )
     */
    public function show(Request $request,PaginatorInterface $paginator): Response
    {
      
      $form=$this->createForm(SearchAlbumotekType::class);
      $form->handleRequest($request);
      
      
      if($form->isSubmitted() && $form->isValid()){
          $repositoriesDisk=$this->repository->search($form->get('mots')->getData());

      }else{
        

         $repositoriesDisk=$paginator->paginate(            
          $this->repository->userDisk($this->getUser()->getId()), /* query NOT result */
          $request->query->getInt('page', 1), /*page number*/
         3 /*limit per page*/); 
           
      }
        return $this->render('accueil/show.html.twig', [
            'repositoriesDisk' => $repositoriesDisk,
            'form'=>$form->createView(),
        ]);
    }
}
