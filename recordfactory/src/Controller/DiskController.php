<?php

namespace App\Controller;

use App\Entity\Disk;
use App\Entity\Song;
use App\Entity\User;
use App\Entity\Rayon;
use App\Form\DiskType;
use App\Form\SongType;
use App\Form\SearchAlbumotekType;
use App\Repository\DiskRepository;
use App\Repository\UserRepository;
use App\Repository\RayonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DiskController extends AbstractController
{

    /**
    * @var DiskRepository
    */
    private $repository;
    /**
    * @var UserRepository
    */
    private $repositoryuser;
    /**
     * @var EntityManagerInterface
     */
     private $em;

   /**
    * constructor
    *
    * @param PropertyRepository $repositoryProperty
    */
   public function __construct(DiskRepository $diskrepository,UserRepository $repositoryuser, EntityManagerInterface $em)
   {
       $this->repository=$diskrepository;
       $this->repositoryuser=$repositoryuser;
       $this->em=$em;
   }
   
    /**
     * @Route("/disk/create", name="disk_create")
     */
    public function create( Request $request,RayonRepository $rayonrepository ): Response
    { 
        $disk = new Disk();
        $form = $this->createForm(DiskType::class, $disk);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
         
            $disk->setUser($this->getUser());
            $this->em->persist($disk);            
            $this->em->flush();
            $this->addFlash('success', 'Disque créé avec succès');
            return $this->redirectToRoute('disk_show');
        }

        return $this->render('disk/create.html.twig', [            
            'form'=>$form->createView(),          
        ]);
      
    }

    /**
     * @Route("/disk/show", name="disk_show" )
     */
    public function show(Request $request,PaginatorInterface $paginator): Response
    {
     
           $repositoryDisk=$paginator->paginate(
           $this->repository->userDisk($this->getUser()->getId()), /* query NOT result */
           $request->query->getInt('page', 1), /*page number*/
           10 /*limit per page*/); 
        
        return $this->render('disk/show.html.twig', [
            'repositoriesDisk' => $repositoryDisk,
        ]);
    }

     /**
     * @Route("/disk/editer/{id}", name="disk_editer" )
     */
    public function editer(Disk $disk, Request $request): Response
    {
     

        $form=$this->createForm(DiskType::class,$disk);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
          
         $this->em->flush();  
          $this->addFlash('success', 'Disque modifié avec succès');
          return $this->redirectToRoute('disk_show');
        }

        return $this->render('disk/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/disk/delete/{id}", name="disk_delete" )
     */
    public function delete(Disk $disk, Request $request): Response
    {
        /* validation d'un jeton csrf envoyé par un formulaire de suppression*/
        if($this->isCsrfTokenValid('delete' . $disk->getId(),$request->get("_token")) ){
            $this->em->remove($disk);
            $this->em->flush();
            $this->addFlash('success', 'Disque supprimé avec succès');        
        }
        
        return $this->redirectToRoute('disk_show');
    }

     /**
     * @Route("/disk/detail/{id}", name="disk_detail" )
     */
    public function detail(Disk $disk, Request $request): Response
    {
        $repositoryDisk=$this->repository->find($disk);      
        /*   dump($repositoryDisk);
           die(); */
         
         return $this->render('disk/detail.html.twig', [
             'repositoryDisk' => $repositoryDisk,
         ]);
    }
    
    /**
     * @Route("/disk/biblioteque", name="disk_biblioteque" )
     */
    public function library(Request $request,PaginatorInterface $paginator): Response
    {
     
        $form=$this->createForm(SearchAlbumotekType::class);
        $form->handleRequest($request);
        
        
        if($form->isSubmitted() && $form->isValid()){
            $repositoriesDisk=$this->repository->search($form->get('mots')->getData());
    
        }else{
            $repositoriesDisk=$this->repository->FindAll(); 
  
        }
          return $this->render('library/show.html.twig', [
              'repositoriesDisk' => $repositoriesDisk,
              'form'=>$form->createView(),
          ]);
    }

}
