<?php

namespace App\Controller;

use App\Entity\Rayon;
use App\Form\RayonType;
use App\Repository\DiskRepository;
use App\Repository\RayonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RayonController extends AbstractController
{
     /**
    * @var RayonRepository
    */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
     private $em;

    public function __construct(RayonRepository $rayonrepository ,EntityManagerInterface $em){
         $this->repository=$rayonrepository;
         $this->em=$em;
    }
    
    
    /**
     * @Route("/rayon/create", name="rayon_create" )
     */
    public function create(Request $request): Response
    {
     
           
        $rayon=new Rayon();
        $form=$this->createForm(RayonType::class,$rayon);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
          $this->em->persist($rayon);
          $this->em->flush(); 
          $this->addFlash('success', 'Genre créé avec succès'); 
          return $this->redirectToRoute('rayon_show');
        }

        return $this->render('rayon/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/rayon/show", name="rayon_show" )
     */
    public function show(Request $request,PaginatorInterface $paginator): Response
    {
     
        $repositoryRayon=$paginator->paginate(
            $this->repository->findAll(), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/);   
        return $this->render('rayon/show.html.twig', [
            'repositoriesRayon' => $repositoryRayon,
        ]);
    }

    /**
     * @Route("/rayon/editer/{id}", name="rayon_editer" )
     */
    public function editer(Rayon $rayon, Request $request): Response
    {
     

        $form=$this->createForm(RayonType::class,$rayon);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
          
          $this->em->flush();  
          $this->addFlash('success', 'Genre modifié avec succès');
          return $this->redirectToRoute('rayon_show');
        }

        return $this->render('rayon/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/rayon/delete/{id}", name="rayon_delete" )
     */
    public function delete(Rayon $rayon, Request $request): Response
    {
        /* validation d'un jeton csrf envoyé par un formulaire de suppression*/
        if($this->isCsrfTokenValid('delete' . $rayon->getId(),$request->get("_token")) ){
            $this->em->remove($rayon);
            $this->em->flush();
            $this->addFlash('success', 'Genre supprimé avec succès');        
        }
        
        return $this->redirectToRoute('rayon_show');
    }
}
