<?php

namespace App\Controller;

use App\Entity\Realizer;
use App\Form\RealizerType;
use App\Repository\RealizerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RealizerController extends AbstractController
{
     /**
    * @var RealizerRepository
    */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
     private $em;

    public function __construct(RealizerRepository $realizerrepository ,EntityManagerInterface $em){
         $this->repository=$realizerrepository;
         $this->em=$em;
    }
    
    
    /**
     * @Route("/realizer/create", name="realizer_create" )
     */
    public function create(Request $request): Response
    {
     
           
        $realizer=new Realizer();
        $form=$this->createForm(RealizerType::class,$realizer);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
          $this->em->persist($realizer);
          $this->em->flush(); 
          $this->addFlash('success', 'Realisateur créé avec succès'); 
          return $this->redirectToRoute('realizer_show');
        }

        return $this->render('realizer/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/realizer/show", name="realizer_show" )
     */
    public function show(Request $request,PaginatorInterface $paginator): Response
    {
        
          $repositoryRealizer=$paginator->paginate(
           $this->repository->findAll(), /* query NOT result */
           $request->query->getInt('page', 1), /*page number*/
          10 /*limit per page*/);   
        return $this->render('realizer/show.html.twig', [
            'repositoriesRealizer' => $repositoryRealizer,
        ]);
    }

    /**
     * @Route("/realizer/editer/{id}", name="realizer_editer" )
     */
    public function editer(Realizer $realizer, Request $request): Response
    {
     

        $form=$this->createForm(RealizerType::class,$realizer);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
          
          $this->em->flush();  
          $this->addFlash('success', 'Realisateur modifié avec succès');
          return $this->redirectToRoute('realizer_show');
        }

        return $this->render('realizer/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/realizer/delete/{id}", name="realizer_delete" )
     */
    public function delete(Realizer $realizer, Request $request): Response
    {
        /* validation d'un jeton csrf envoyé par un formulaire de suppression*/
        if($this->isCsrfTokenValid('delete' . $realizer->getId(),$request->get("_token")) ){
            $this->em->remove($realizer);
            $this->em->flush();
            $this->addFlash('success', 'Realisateur supprimé avec succès');        
        }
        
        return $this->redirectToRoute('realizer_show');
    }
}
