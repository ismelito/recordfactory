<?php

namespace App\Controller;

use App\Entity\Song;
use App\Form\SongType;
use App\Repository\SongRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SongController extends AbstractController
{
     /**
    * @var SongRepository
    */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
     private $em;

    public function __construct(SongRepository $songrepository ,EntityManagerInterface $em){
         $this->repository=$songrepository;
         $this->em=$em;
    }
    
    
    /**
     * @Route("/song/create", name="song_create" )
     */
    public function create(Request $request): Response
    {
     
        
        $song=new Song();
        $form=$this->createForm(SongType::class,$song);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
          $this->em->persist($song);
          $this->em->flush(); 
          $this->addFlash('success', 'Chanson créé avec succès'); 
          return $this->redirectToRoute('song_show');
        }

        return $this->render('song/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/song/show", name="song_show" )
     */
    public function show(Request $request,PaginatorInterface $paginator): Response
    {
       
          $repositoriesSong=$paginator->paginate(
           $this->repository->songDiskUser($this->getUser()->getId()), /* query NOT result */
           $request->query->getInt('page', 1), /*page number*/
          10/*limit per page*/);   
          return $this->render('song/show.html.twig', [
            'repositoriesSong' => $repositoriesSong,
        ]);
    }

    /**
     * @Route("/song/editer/{id}", name="song_editer" )
     */
    public function editer(Song $realizer, Request $request): Response
    {
     

        $form=$this->createForm(SongType::class,$realizer);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
          
          $this->em->flush();  
          $this->addFlash('success', 'Chanson modifié avec succès');
          return $this->redirectToRoute('song_show');
        }

        return $this->render('song/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/song/delete/{id}", name="song_delete" )
     */
    public function delete(Song $song, Request $request): Response
    {
        /* validation d'un jeton csrf envoyé par un formulaire de suppression*/
        if($this->isCsrfTokenValid('delete' . $song->getId(),$request->get("_token")) ){
            $this->em->remove($song);
            $this->em->flush();
            $this->addFlash('success', 'Chanson supprimé avec succès');        
        }
        
        return $this->redirectToRoute('song_show');
    }

   
}
