<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        // create 5 products! Bam!
        for ($i = 0; $i < 5; $i++) {
           
            $user = new User();
            $user->setEmail($faker->email());

            $password = $this->encoder->encodePassword($user, 'password'. $i);
            $user->setPassword($password);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
