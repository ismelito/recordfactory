<?php

namespace App\Entity;

use App\Repository\RayonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RayonRepository::class)
 * @UniqueEntity("name", message="Ce genre musical existe déjà ")
 */
class Rayon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Regex("/^[A-Z]/",message="Le genre musical doit être écrit en lettres majuscules.")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Disk::class, mappedBy="rayon")
     */
    private $disk;

    /**
     * @Assert\Length(
     *      min = 10,
     *      max = 500,
     *      minMessage = "Votre description doit être au moins {{ limit }} caractères longs",
     *      maxMessage = "Votre description ne peut pas être plus long que {{ limit }} caractères"
     * )
     * @ORM\Column(type="text")
     */
    private $description;

    public function __construct()
    {
        $this->disk = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Disk[]
     */
    public function getDisk(): Collection
    {
        return $this->disk;
    }

    public function addDisk(Disk $disk): self
    {
        if (!$this->disk->contains($disk)) {
            $this->disk[] = $disk;
            $disk->setRayon($this);
        }

        return $this;
    }

    public function removeDisk(Disk $disk): self
    {
        if ($this->disk->removeElement($disk)) {
            // set the owning side to null (unless already changed)
            if ($disk->getRayon() === $this) {
                $disk->setRayon(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
