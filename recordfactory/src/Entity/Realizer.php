<?php

namespace App\Entity;

use App\Repository\RealizerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RealizerRepository::class)
 */
class Realizer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="integer")
     */
    private $diskRealizer;

    /**
     * @ORM\OneToMany(targetEntity=Disk::class, mappedBy="realizer")
     */
    private $disks;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function __construct()
    {
        $this->createAt= new \DateTime();
        $this->disks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDiskRealizer(): ?int
    {
        return $this->diskRealizer;
    }

    public function setDiskRealizer(int $diskRealizer): self
    {
        $this->diskRealizer = $diskRealizer;

        return $this;
    }

    /**
     * @return Collection|Disk[]
     */
    public function getDisk(): Collection
    {
        return $this->disks;
    }

    public function addDisk(Disk $disks): self
    {
        if (!$this->disk->contains($disks)) {
            $this->disks[] = $disks;
            $disks->setRealizer($this);
        }

        return $this;
    }

    public function removeDisk(Disk $disk): self
    {
        if ($this->disk->removeElement($disk)) {
            // set the owning side to null (unless already changed)
            if ($disk->getRealizer() === $this) {
                $disk->setRealizer(null);
            }
        }

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getTelephone(): ?int
    {
        return $this->telephone;
    }

    public function setTelephone(int $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
