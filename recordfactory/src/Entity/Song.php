<?php

namespace App\Entity;



use App\Entity\Disk;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SongRepository;
use Doctrine\Common\Collections\Collection;



/**
 * @ORM\Entity(repositoryClass=SongRepository::class)
 */
class Song 
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $duration;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $singer;
  

    /**
     * @ORM\ManyToOne(targetEntity=Disk::class, inversedBy="songs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $disk;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }    

    public function getDuration(): ?float
    {
        return $this->duration;
    }

    public function setDuration(?float $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function getSinger(): ?string
    {
        return $this->singer;
    }

    public function setSinger(string $singer): self
    {
        $this->singer = $singer;

        return $this;
    }

    public function getDisk(): ?Disk
   {
       return $this->disk;
   }

   public function setDisk(?Disk $disk): self
   {
       $this->disk = $disk;

       return $this;
   }
  

   
 
}
