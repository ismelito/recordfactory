<?php

namespace App\Form;

use App\Entity\Disk;
use App\Entity\Song;
use App\Entity\Rayon;
use App\Entity\Realizer;
use App\Repository\RayonRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class DiskType extends AbstractType
{
    /**
     * @var RayonRepository
     */

     private $rayonRepository;

    public function __construct(RayonRepository $rayonRepository)
    {
        $this->rayonRepository=$rayonRepository;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,["label"=>"Titre"])            
            ->add('description')
            ->add('rayon',EntityType::class, [
                // looks for choices from this entity
                'class' => Rayon::class,            
                // uses the User.username property as the visible option string
                'choice_label' => 'name', 
                'label'=>'Genre',           
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('realizer',EntityType::class, [
                // looks for choices from this entity
                'class' => Realizer::class,            
                // uses the User.username property as the visible option string
                'choice_label' => 'name',
                'label'=>'Realisateur',              
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ]);
           
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Disk::class,
        ]);
    }

    public function genre(){
        $repo=$this->rayonRepository->findAll();
        $array_rayons=[];
        foreach($repo as $ray){
           array_push($array_rayons,$ray->getName());
        }
       
        return $array_rayons;
    }
}
