<?php

namespace App\Form;

use App\Entity\Disk;
use App\Entity\Song;
use App\Entity\User;
use App\Repository\DiskRepository;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;




class SongType extends AbstractType
{   
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    

    public function buildForm(FormBuilderInterface $builder, array $options)
    {   

        $builder
            ->add('name',null,["label"=>"Nom"])
            ->add('description',null,["label"=>"Description"])
            ->add('duration',null,["label"=>"Durée"])
            ->add('singer',null,["label"=>"Chanteur"])
            ->add('disk',EntityType::class, [
                // looks for choices from this entity
                'class' => Disk::class,            
                // uses the User.username property as the visible option string
                'choice_label' => 'name',   
                       
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
               'query_builder' =>function(DiskRepository $diskrepository)  {
                    $qb = $diskrepository->createQueryBuilder('di');
                    $value=$this->security->getUser()->getId(); 
                    return $qb
                    ->andWhere('us.id = :val')
                    ->setParameter('val', $value)
                    ->innerjoin('di.user', 'us')            
                    
                    ;
                },
            ]
        );
    }

 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Song::class,
        ]);
    }
}
