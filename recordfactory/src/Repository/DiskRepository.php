<?php

namespace App\Repository;

use App\Entity\Disk;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Disk|null find($id, $lockMode = null, $lockVersion = null)
 * @method Disk|null findOneBy(array $criteria, array $orderBy = null)
 * @method Disk[]    findAll()
 * @method Disk[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiskRepository extends ServiceEntityRepository
{
  

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Disk::class);
        
    }

    public function search($term)
    {
        return $this->createQueryBuilder('di')
            ->andWhere('di.name LIKE :searchTerm 
                OR re.name LIKE :searchTerm              
                OR ra.name LIKE :searchTerm')
            ->leftJoin('di.rayon', 'ra')
            ->leftJoin('di.realizer', 're')
            ->setParameter('searchTerm', '%'.$term.'%')
            ->getQuery()
            ->execute();
    }

    public function userDisk($value)
    {        
        return $this->createQueryBuilder('di')
            ->andWhere('us.id = :val')
            ->setParameter('val', $value)
            ->innerjoin('di.user', 'us')            
            ->getQuery()
            ->execute();
    }
    
    
   

    // /**
    //  * @return Disk[] Returns an array of Disk objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Disk
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
