<?php

namespace App\Repository;

use App\Entity\Realizer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Realizer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Realizer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Realizer[]    findAll()
 * @method Realizer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealizerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Realizer::class);
    }

    // /**
    //  * @return Realizer[] Returns an array of Realizer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Realizer
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
